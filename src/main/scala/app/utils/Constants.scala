package app.utils

import scala.concurrent.duration._

object Constants {
  val TOKEN_ACTOR_PREFIX = "token-"
  val USER_ACTOR_PREFIX = "user-"
  val USER_BASE_ACTOR_PATH = "/user/"

  //Below can be in configuration file
  val STOP_ACTOR_TIMEOUT = 5 minutes
  val HOST = "localhost"
  val PORT = 8080
}
