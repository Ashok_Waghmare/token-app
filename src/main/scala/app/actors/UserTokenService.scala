package app.actors

import java.time._

import akka.actor.{Actor, Props, ReceiveTimeout}
import app.utils.Constants.STOP_ACTOR_TIMEOUT
import app.actors.UserTokenService.GetUserToken
import app.exceptions.Exceptions.{BadRequestError, InvalidValue}
import app.model.{User, UserToken}

object UserTokenService {
  def props: Props = Props(new UserTokenService())

  final case class GetUserToken(user: User)
}

class UserTokenService extends Actor {
  context.setReceiveTimeout(STOP_ACTOR_TIMEOUT)

  override def receive: Receive = {
    case ReceiveTimeout =>
      context stop self
    case GetUserToken(user) if user.userId.startsWith("A") =>
      sender() ! InvalidValue(s"invalid value: ${user.userId}")
    case GetUserToken(user) =>
      val date = LocalDateTime.now().withNano(0).atZone(
        ZoneId.of("UTC")
      ).toString.replace("[UTC]", "")
      sender() ! UserToken(s"${user.userId}_${date}")
    case _ =>
      sender() ! BadRequestError("unsupported msg type")
  }
}
