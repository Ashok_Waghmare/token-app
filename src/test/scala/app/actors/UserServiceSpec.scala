package app.actors

import akka.actor.ReceiveTimeout
import akka.testkit.{ImplicitSender, TestKitBase, TestProbe}
import app.exceptions.Exceptions.{AuthenticationFailed, BadRequestError}
import app.model.{Credentials, User}
import app.utils.SystemContext
import org.scalatest.{FreeSpec, Matchers}

class UserServiceSpec extends FreeSpec with Matchers with TestKitBase with ImplicitSender with SystemContext {
  private val userServiceActor = system.actorOf(UserService.props)

  "User service actor" - {

    "should return user on valid credentials" in {
      userServiceActor ! Credentials("username", "USERNAME")
      expectMsg(User("username"))
    }

    "should return authentication failed on invalid credentials" in {
      userServiceActor ! Credentials("username", "USERNAE")
      expectMsg(AuthenticationFailed("authentication failed!!"))
    }

    "should return unsupported msg for any other messages" in {
      userServiceActor ! "username,UserName"
      expectMsg(BadRequestError("unsupported msg type"))
    }

    "should stop on receive timeout message" in {
      val testProbe = TestProbe()
      testProbe watch userServiceActor
      userServiceActor ! ReceiveTimeout
      testProbe.expectTerminated(userServiceActor)
    }

  }

}
