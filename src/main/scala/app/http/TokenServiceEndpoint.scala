package app.http

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.server.Directives.{complete, _}
import app.model.Credentials
import app.services.SimpleAsyncTokenService
import spray.json.DefaultJsonProtocol._

import scala.util.{Failure => ScalaFailure, Success => ScalaSuccess}

class TokenServiceEndpoint(simpleAsyncTokenService: SimpleAsyncTokenService) {
  implicit val credentialFormat = jsonFormat2(Credentials)

  val route= post {
    path("token") {
      entity(as[Credentials]) { credential =>
        onComplete(simpleAsyncTokenService.requestToken(credential)) {
          case ScalaSuccess(value) => complete(value.token)
          case ScalaFailure(ex) => complete(ex.getMessage)
        }
      }
    }
  }

}
