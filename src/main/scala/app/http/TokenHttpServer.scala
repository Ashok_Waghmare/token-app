package app.http

import akka.http.scaladsl.server.Route
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.stream.ActorMaterializer
import app.model.Credentials
import app.utils.SystemContext
import spray.json.DefaultJsonProtocol.jsonFormat2
import spray.json.DefaultJsonProtocol._
import app.utils.Constants.HOST
import app.utils.Constants.PORT

import scala.concurrent.Future

class TokenHttpServer(route: Route) extends SystemContext {
  implicit val materializer = ActorMaterializer()
  implicit val credentialFormat = jsonFormat2(Credentials)

  def start: Future[ServerBinding] = Http().bindAndHandle(route, HOST, PORT)
}
