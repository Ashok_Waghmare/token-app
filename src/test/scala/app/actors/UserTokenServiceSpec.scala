package app.actors

import akka.actor.ReceiveTimeout
import akka.testkit.{ImplicitSender, TestKitBase, TestProbe}
import app.actors.UserTokenService.GetUserToken
import app.exceptions.Exceptions.{BadRequestError, InvalidValue}
import app.model.{User, UserToken}
import app.utils.SystemContext
import org.scalatest.{FreeSpec, Matchers}

class UserTokenServiceSpec extends FreeSpec with Matchers with TestKitBase with ImplicitSender with SystemContext {
  private val userTokenService = system.actorOf(UserTokenService.props)

  "UserToken service actor" - {

    "should return InvalidValue on userId starts with 'A'" in {
      userTokenService ! GetUserToken(User("A-user"))
      expectMsg(InvalidValue("invalid value: A-user"))
    }

    "should return User token for start with small 'a' user id" in {
      userTokenService ! GetUserToken(User("a-user"))
      expectMsgPF() {
        case userToken: UserToken if isValidUserToken(userToken.token) =>
      }
    }

    "should return User token for other user values" in {
      userTokenService ! GetUserToken(User("user"))
      expectMsgPF() {
        case userToken: UserToken if isValidUserToken(userToken.token) =>
      }
    }

    "should return unsupported msg for any other messages" in {
      userTokenService ! "randomMsg"
      expectMsg(BadRequestError("unsupported msg type"))
    }

    "should stop on receive timeout message" in {
      val testProbe = TestProbe()
      testProbe watch userTokenService
      userTokenService ! ReceiveTimeout
      testProbe.expectTerminated(userTokenService)
    }

  }

  private def isValidUserToken(token: String): Boolean = {
    token.contains("user_") && token.matches(".*_\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}Z")
  }

}
