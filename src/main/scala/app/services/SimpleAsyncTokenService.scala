package app.services

import app.model.{Credentials, UserToken}

import scala.concurrent.Future

trait SimpleAsyncTokenService {
  def requestToken(credentials: Credentials): Future[UserToken]
}
