package app.http

import akka.http.scaladsl.model._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import app.utils.Dependencies._
import org.scalatest.{FreeSpec, Matchers}

class TokenServiceEndpointSpec extends FreeSpec with Matchers with ScalatestRouteTest {


  "Token service endpoint" - {
    "should return token value" in {
      val validCredential = "{\"username\":\"xyz\",\"password\":\"XYZ\"}"
      buildPostRequest(validCredential) ~> tokenServiceEndpoint.route ~> check {
        responseAs[String] should startWith("xyz_")
        responseAs[String] should endWith("Z")
      }
    }

    "should return authentication failed message on wrong credentials" in {
      val wrongCredentials = "{\"username\":\"xyz\",\"password\":\"xyz\"}"
      buildPostRequest(wrongCredentials) ~> tokenServiceEndpoint.route ~> check {
        responseAs[String] shouldEqual  "authentication failed!!"
      }
    }

    "should return invalid value for invalid user name" in {
      val invalidCredentials = "{\"username\":\"A-user\",\"password\":\"A-USER\"}"
      buildPostRequest(invalidCredentials) ~> tokenServiceEndpoint.route ~> check {
        responseAs[String] shouldEqual  "invalid value: A-user"
      }
    }
  }

  def buildPostRequest(json: String) = HttpRequest(HttpMethods.POST, uri = "/token",
    entity = HttpEntity(MediaTypes.`application/json`, json))

}
