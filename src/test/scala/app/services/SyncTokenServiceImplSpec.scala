package app.services

import akka.actor.ActorRef
import akka.testkit.{ImplicitSender, TestKitBase}
import app.actors.UserService
import app.exceptions.Exceptions.{AuthenticationFailed, InvalidValue}
import app.model.{Credentials, User, UserToken}
import app.utils.SystemContext
import org.scalatest.{FreeSpec, Matchers}

class SyncTokenServiceImplSpec extends FreeSpec with Matchers with TestKitBase with ImplicitSender with SystemContext {
  val syncTokenServiceImpl = new SyncTokenServiceImpl(system)

  "SyncToken Service" - {

    "should return user on success of authentication" in {
      syncTokenServiceImpl.authenticate(Credentials("username", "USERNAME")) shouldBe User("username")
    }

    "should throw an exception on failure of authentication" in {
      an[AuthenticationFailed] should be thrownBy syncTokenServiceImpl.authenticate(Credentials("username", "password"))
    }

    "should return token for an user" in {
      syncTokenServiceImpl.issueToken(User("username")) shouldBe a[UserToken]
    }

    "should throw an exception on invalid user name" in {
      an[InvalidValue] should be thrownBy  syncTokenServiceImpl.issueToken(User("A-username"))
    }

    "should return always actor reference" in {
      val result = syncTokenServiceImpl.findOrCreateServiceActor("username", UserService.props)(system)
      result shouldBe a[ActorRef]
    }

    "should return token" in {
      val result = syncTokenServiceImpl.requestToken(Credentials("username", "USERNAME"))
      result shouldBe a[UserToken]
    }

  }

}
