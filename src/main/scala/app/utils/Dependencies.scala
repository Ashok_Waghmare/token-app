package app.utils

import app.http.{TokenHttpServer, TokenServiceEndpoint}
import app.services.{AsyncTokenServiceImpl, SimpleAsyncTokenServiceImpl}

object Dependencies extends SystemContext {
  lazy val asyncTokenServiceImpl = new AsyncTokenServiceImpl(system)
  lazy val simpleAsyncTokenServiceImpl = new SimpleAsyncTokenServiceImpl(asyncTokenServiceImpl)
  lazy val tokenServiceEndpoint = new TokenServiceEndpoint(simpleAsyncTokenServiceImpl)
  lazy val tokenHttpServer = new TokenHttpServer(tokenServiceEndpoint.route)
}
