# Token App
-------------

This is service to authenticate, and grant a token to an user.

#Prerequisite
-------------
Make sure sbt is installed (with version 1.2.6) 

## How to ?
--------------

To run the tests
        
        sbt test

To start the application

        sbt run

To get Token

    curl --header "Content-Type: application/json" --request POST --data '{"username":"user","password":"USER"}' http://localhost:8080/token
        

## Used Libraries

* Akka actors- 
    This application service is based on Akka actors(to achieve high concurrency).  

* Akka Http - 
REST endpoint is exposed using Akka Http. Since i did not want to develop full web application, that makes me to choose 
Akka Http over other web frameworks. 

* Akka stream - To bind routes to http server it needs ActorMaterializer, so Akka stream is used.

* akka-http-spray-json - It is used to convert input credential json to object.

* akka-testkit - To test the Actors

* akka-http-testkit - To test the http routes

* scalatest - To add scala tests, Free like spec and for Matchers. 





  
 

 