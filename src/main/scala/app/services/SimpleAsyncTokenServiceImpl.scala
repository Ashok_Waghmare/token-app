package app.services

import app.model.{Credentials, UserToken}

import scala.concurrent.Future

class SimpleAsyncTokenServiceImpl(asyncTokenService: AsyncTokenService) extends SimpleAsyncTokenService {

  override def requestToken(credentials: Credentials): Future[UserToken] = asyncTokenService.requestToken(credentials)

}
