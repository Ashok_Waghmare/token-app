package app.exceptions

trait ExceptionHandler {

  def handleException(e: Throwable) = {
    //can add logs for extra info
    throw e
  }

}
