package app.services

import akka.actor.{ActorNotFound, ActorRef, ActorSystem, Props}
import akka.pattern.ask
import app.actors.UserTokenService.GetUserToken
import app.exceptions.ExceptionHandler
import app.exceptions.Exceptions.HandledErrorBase
import app.model.Credentials

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import app.utils.Constants.USER_BASE_ACTOR_PATH

trait TokenServiceBase extends ExceptionHandler {

  def msgToUserService(userService: ActorRef, credentials: Credentials): Future[Any] = {
    userService.ask(credentials)(5 seconds)
  }

  def msgToTokenService(tokenService: ActorRef, getUserToken: GetUserToken): Future[Any] = {
    tokenService.ask(getUserToken)(5 seconds)
  }

  def handleResponse[T](res: Any): T = {
    res match {
      case e: HandledErrorBase => handleException(e)
      case response: T => response
    }
  }

  def findOrCreateServiceActorAsync(name: String, props: Props)(system: ActorSystem): Future[Option[ActorRef]] = {
    val timeout = 0.1 seconds
    val selection = system.actorSelection(s"$USER_BASE_ACTOR_PATH$name")
    selection.resolveOne(timeout).map(Some(_)).recover {
      case _: ActorNotFound => Some(system.actorOf(props, name))
    }
  }

  def findOrCreateServiceActor(name: String, props: Props)(system: ActorSystem): ActorRef = {
    val timeout = 0.1 seconds
    val fut = findOrCreateServiceActorAsync(name, props)(system)
    val refOpt = Await.result(fut, timeout)
    refOpt match {
      case Some(ref) => ref
      case None => system.actorOf(props, name)
    }
  }

}
