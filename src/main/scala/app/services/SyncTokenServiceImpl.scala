package app.services

import akka.actor.{ActorRef, ActorSystem}
import app.actors.UserTokenService.GetUserToken
import app.actors.{UserService, UserTokenService}
import app.model.{Credentials, User, UserToken}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import app.utils.Constants._


class SyncTokenServiceImpl(system: ActorSystem) extends SyncTokenService with TokenServiceBase {

  override def authenticate(credentials: Credentials): User = {
    val actorName = s"$USER_ACTOR_PREFIX${credentials.username}"
    val actorRef: ActorRef = findOrCreateServiceActor(actorName, UserService.props)(system)
    val user: Future[User] = msgToUserService(actorRef, credentials) map handleResponse[User]
    Await.result(user, 5 seconds)
  }

  override def issueToken(user: User): UserToken = {
    val actorName = s"$TOKEN_ACTOR_PREFIX${user.userId}"
    val actorRef: ActorRef = findOrCreateServiceActor(actorName, UserTokenService.props)(system)
    val futUser: Future[UserToken] = msgToTokenService(actorRef, GetUserToken(user)) map handleResponse[UserToken]
    Await.result(futUser, 5 seconds)
  }

}
