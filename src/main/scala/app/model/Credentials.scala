package app.model

case class Credentials(username: String, password: String)
