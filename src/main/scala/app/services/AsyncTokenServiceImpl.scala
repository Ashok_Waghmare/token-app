package app.services

import akka.actor.ActorSystem
import app.actors.UserTokenService.GetUserToken
import app.actors.{UserService, UserTokenService}
import app.exceptions.Exceptions.ServiceNotFoundException
import app.model.{Credentials, User, UserToken}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import app.utils.Constants._

class AsyncTokenServiceImpl(system: ActorSystem) extends AsyncTokenService with TokenServiceBase {

  override def authenticate(credentials: Credentials): Future[User] = {
    val nameOfActor = s"$USER_ACTOR_PREFIX${credentials.username}"
    findOrCreateServiceActorAsync(nameOfActor, UserService.props)(system) flatMap {
      case Some(actorRef) => msgToUserService(actorRef, credentials) map handleResponse[User]
      case _ => throw ServiceNotFoundException("unable to locate or create service")
    }
  }

  override def issueToken(user: User): Future[UserToken] = {
    val nameOfActor = s"$TOKEN_ACTOR_PREFIX${user.userId}"
    findOrCreateServiceActorAsync(nameOfActor, UserTokenService.props)(system) flatMap {
        case Some(actorRef) => msgToTokenService(actorRef, GetUserToken(user)) map handleResponse[UserToken]
        case _ => throw ServiceNotFoundException("unable to locate or create service")
    }
  }

}
