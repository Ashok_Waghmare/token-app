package app.services

import akka.testkit.{ImplicitSender, TestKitBase}
import app.model.{Credentials, UserToken}
import app.utils.SystemContext
import org.scalatest.{FreeSpec, Matchers}

import scala.concurrent.Await
import scala.concurrent.duration._

class SimpleAsyncTokenServiceImplSpec extends FreeSpec with Matchers with TestKitBase with ImplicitSender with SystemContext {
  val asyncTokenServiceImpl = new AsyncTokenServiceImpl(system)
  val simpleAsyncTokenServiceImpl = new SimpleAsyncTokenServiceImpl(asyncTokenServiceImpl)

  "SimpleAsyncTokenService" - {
    "should return token" in {
      val result = Await.result(simpleAsyncTokenServiceImpl.requestToken(Credentials("username", "USERNAME")), 5 seconds)
      result shouldBe a[UserToken]
    }
  }

}
