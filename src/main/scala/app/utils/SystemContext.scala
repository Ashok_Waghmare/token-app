package app.utils

import akka.actor.ActorSystem

trait SystemContext {
   implicit lazy val system: ActorSystem = ActorSystem("Token-System")
}
