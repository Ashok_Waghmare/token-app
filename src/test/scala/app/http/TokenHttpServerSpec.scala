package app.http

import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.http.scaladsl.model._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import app.utils.Dependencies._
import org.scalatest.{FreeSpec, Matchers}

import scala.concurrent.Await
import scala.concurrent.duration._

class TokenHttpServerSpec extends FreeSpec with Matchers with ScalatestRouteTest {
  implicit val executionContext = system.dispatcher

  "TokenHttp server" - {
    "should start http server" in {
      val response = Await.result(tokenHttpServer.start, 1 seconds)
      response shouldBe a[ServerBinding]
      val host = response.localAddress.getHostString
      val port = response.localAddress.getPort

      val request  = HttpRequest(HttpMethods.POST, uri = s"http://$host:$port/token",
        entity = HttpEntity(MediaTypes.`application/json`, "{\"username\":\"xyz\",\"password\":\"XYZ\"}"))

      val res = Await.result(Http().singleRequest(request), 1 seconds)
      res.status should ===(StatusCodes.OK)

      response.unbind()
    }
  }
}
