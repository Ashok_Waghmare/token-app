package app.exceptions

object Exceptions {

  class HandledErrorBase(msg: String) extends RuntimeException(msg)

  case class BadRequestError(msg: String) extends HandledErrorBase(msg)

  case class InvalidValue(msg: String) extends HandledErrorBase(msg)

  case class AuthenticationFailed(msg: String) extends HandledErrorBase(msg)

  case class ServiceNotFoundException(msg: String) extends HandledErrorBase(msg)
}
