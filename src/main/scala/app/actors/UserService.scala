package app.actors

import akka.actor.{Actor, Props, ReceiveTimeout}
import app.exceptions.Exceptions.{AuthenticationFailed, BadRequestError}
import app.model.{Credentials, User}
import app.utils.Constants._

object UserService {
  def props: Props = Props(new UserService())
}

class UserService extends Actor {
  context.setReceiveTimeout(STOP_ACTOR_TIMEOUT)

  override def receive: Receive = {
    case ReceiveTimeout =>
      context stop self
    case credentials: Credentials if credentials.username.toUpperCase == credentials.password =>
      sender() ! User(credentials.username)
    case _ : Credentials =>
      sender() ! AuthenticationFailed("authentication failed!!")
    case _ =>
      sender() ! BadRequestError("unsupported msg type")
  }

}
