package app.services

import akka.actor.ActorRef
import akka.testkit.{ImplicitSender, TestKitBase}
import app.actors.UserService
import app.exceptions.Exceptions.{AuthenticationFailed, InvalidValue}
import app.model.{Credentials, User, UserToken}
import app.utils.SystemContext
import org.scalatest.{FreeSpec, Matchers}

import scala.concurrent.Await
import scala.concurrent.duration._

class AsyncTokenServiceImplSpec extends FreeSpec with Matchers with TestKitBase with ImplicitSender with SystemContext {
  val asyncTokenServiceImpl = new AsyncTokenServiceImpl(system)

  "AsyncToken Service" - {

    "should return user on success of authentication" in {
      val result = Await.result(asyncTokenServiceImpl.authenticate(Credentials("username", "USERNAME")), 5 seconds)
      result shouldBe User("username")
    }

    "should throw an exception on failure of authentication" in {
      an[AuthenticationFailed] should be thrownBy
        Await.result(asyncTokenServiceImpl.authenticate(Credentials("username", "password")), 5 seconds)
    }

    "should return token for an user" in {
      val result = Await.result(asyncTokenServiceImpl.issueToken(User("username")), 5 seconds)
      result shouldBe a[UserToken]
    }

    "should throw an exception on invalid user name" in {
      an[InvalidValue] should be thrownBy Await.result(asyncTokenServiceImpl.issueToken(User("A-username")), 5 seconds)
    }

    "should return always actor reference" in {
      val result =  Await.result(
        asyncTokenServiceImpl.findOrCreateServiceActorAsync("username", UserService.props)(system), 5 seconds
      )
      result.get shouldBe a[ActorRef]
    }

    "should return token" in {
      val result = Await.result(asyncTokenServiceImpl.requestToken(Credentials("username", "USERNAME")), 5 seconds)
      result shouldBe a[UserToken]
    }

  }

}
