name := "token-app"
version := "1.0"
scalaVersion := "2.12.7"

libraryDependencies ++= Seq("com.typesafe.akka" %% "akka-actor" % "2.5.18",
"com.typesafe.akka" %% "akka-http"   % "10.1.5",
"com.typesafe.akka" %% "akka-stream" % "2.5.18",
"com.typesafe.akka" %% "akka-http-spray-json" % "10.1.5",

"org.scalatest" % "scalatest_2.12" % "3.0.3" % Test,
"com.typesafe.akka" %% "akka-testkit" % "2.5.18" % Test,
"com.typesafe.akka" %% "akka-http-testkit" % "10.1.5" % Test,

//TODO: akka-stream-testkit comes with akka-http-testkit but older version, as such stream test is not in use.
"com.typesafe.akka" %% "akka-stream-testkit" % "2.5.18" % Test
)
